#include "Comparador.hpp"

double Comparador::operator()(const Ponto &p1, const Ponto &p2)
{   
    return p1.x_ > p2.x_;
}