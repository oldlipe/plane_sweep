#ifndef __COMPARADOR_HPP__
#define __COMPARADOR_HPP__
#include"Ponto.hpp"

class Comparador
{
    public:
        double operator() (const Ponto& p1, const Ponto& p2);          
    private:
};
#endif // __COMPARADOR_HPP__