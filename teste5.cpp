/**
 * @name Felipe Carvalho e Paulo
 * 
 * @description Algoritmo Plane Sweep
 * */
#include "PlaneSweep.hpp"
#include "EventQueue.hpp"
#include <iostream>
#include <functional>

int main()
{
    std::vector<Intervalo> v {
         { Ponto {-2,2}, Ponto {1,3} },
         { Ponto {-3,4}, Ponto {2,4} },
         { Ponto {0,6}, Ponto {5,8} },
         { Ponto {4, 1}, Ponto {9,2}},
         { Ponto {3, 5}, Ponto {7,1}},
         { Ponto {9, 4}, Ponto {11,3}},
         { Ponto {10, 4}, Ponto {11,2}}
    };
    PlaneSweep pS = PlaneSweep();

    std::vector<SweepLineStatus> Total = eventScheduleList(v);

    
    // Para converter para 
    std::priority_queue<SweepLineStatus, std::vector<SweepLineStatus>, CompareHeight > pQ;
    
    for(auto &i : Total)
    {
        pQ.push(i);
    }

    std::cout << " " << std::endl; 

   
    pS.bIntersect(pQ);

    return 0;

}