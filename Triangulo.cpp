#include "Triangulo.hpp"

Triangulo::Triangulo(Ponto *p1, Ponto *p2, Ponto *p3)
{
    p1_ = p1;
    p2_ = p2;
    p3_ = p3;
}


double Triangulo::area()
{
    double s = ((p1_->x_*p2_->y_)-(p1_->y_*p2_->x_)+(p1_->y_*p3_->x_) -
    (p1_->x_*p3_->y_)+(p2_->x_*p3_->y_)-(p2_->y_*p3_->x_))*0.5;

    return(s);
}

