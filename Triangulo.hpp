#ifndef __TRIANGULO_HPP__
#define __TRIANGULO_HPP__
#include "Ponto.hpp"

class Triangulo
{
    public:
        // construtor
        Triangulo(Ponto *p1, Ponto *p2, Ponto *p3);
    
        // atributos
        Ponto *p1_;
        Ponto *p2_;
        Ponto *p3_;

        double area();

        double area(Ponto *p1, Ponto *p2, Ponto *p3);

};

#endif // __TRIANGULO_HPP__