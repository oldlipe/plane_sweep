#ifndef __EVENTQUEUE_HPP__
#define __EVENTQUEUE_HPP__
#include "SweepLineStatus.hpp"
#include <vector>
#include <queue>
#include <algorithm>


/**
 * @name filaEventos
 * 
 * @param vector<Intervalo> Vetor correspondente ao conjunto de segmentos 
 * 
 * @return vector <SweepLineStatus> Vetor com as abscissas ordenadas
 * */
std::vector<SweepLineStatus> eventScheduleList(const std::vector<Intervalo> v);

/**
 * @name simpleChange
 * 
 * @decription Método simples para conversão de vector para priority_queue
 * 
 * @param vector<SweepLineStatus> Vetor de Segmentos atuais na linha imaginária 
 * 
 * @return vector <SweepLineStatus> Vetor com as abscissas ordenadas
 * */
 std::priority_queue<SweepLineStatus, 
    std::vector<SweepLineStatus>, 
    CompareHeight > simpleConverter(std::priority_queue<SweepLineStatus, 
                                std::vector<SweepLineStatus>, 
                                CompareHeight >);



#endif // __EVENTQUEUE_HPP__