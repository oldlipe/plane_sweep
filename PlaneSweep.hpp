#ifndef __PLANESWEEP_HPP__
#define __PLANESWEEP_HPP__
#include "SweepLineStatus.hpp"
#include"Intersects.hpp"
#include <vector>
#include <queue>
#include <set>


class PlaneSweep
{   
    public:
        /**
         * @name sIntersect Método responsável por varrer o plano, explicado por
         * Michael Ian Shamos
         * 
         * @param vector<SweepLineStatus> Vetor com os segmentos ordenados
         * 
         * @return bool Retorna True caso tenha alguma intersecção dado um
         * conjunto de segmentos. False para nenhuma intersecção entre objetos.
         * */
        bool sIntersect( const std::vector<SweepLineStatus> Total);
 

        /**
         * @name sIntersect Método responsável por varrer o plano, explicado por
         * Michael Ian Shamos
         * 
         * @param vector<SweepLineStatus> Vetor com os segmentos ordenados
         * 
         * @return bool Retorna True caso tenha alguma intersecção dado um
         * conjunto de segmentos. False para nenhuma intersecção entre objetos.
         * */
        int bIntersect(
                std::priority_queue<SweepLineStatus, 
                        std::vector<SweepLineStatus>, 
                        CompareHeight > pQ);

};
#endif // __PLANESWEEP_HPP__