#ifndef __SEGMENTO_HPP__
#define __SEGMENTO_HPP__
#include "Ponto.hpp"

class Segmento
{
    public:

        Ponto *p1_;
        Ponto *p2_;

        Segmento(Ponto &p1, Ponto &p2);

};


#endif // __SEGMENTO_HPP__
