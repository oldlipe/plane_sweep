#ifndef __SWEEPLINESTATUS_HPP__
#define __SWEEPLINESTATUS_HPP__
#include "Ponto.hpp"
#include "Intervalo.hpp"

/**
 * @name SweepLineStatus
 * 
 * @description Struct do segmento da sweep line, por exemplo,
 * a flag {esq_} diz respeito se o ponto extremo em T é esquerdo ou direito.
 * 
 **/
struct SweepLineStatus
{
    Ponto p_;
    Intervalo s_;
    bool esq_;

    bool operator< (const SweepLineStatus &segmento2) const {
        return p_.x < segmento2.p_.x;
    }

};

struct CompareHeight { 
    bool operator()(SweepLineStatus const &segmento1, SweepLineStatus const &segmento2) 
    { 
        // return "true" if "p1" is ordered  
        // before "p2", for example: 
        return segmento1.p_.x > segmento2.p_.x;
    } 
}; 
  
#endif  //__SWEEPLINESTATUS_HPP__


