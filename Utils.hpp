#ifndef __UTILS_HPP__
#define __UTILS_HPP__
#include "Ponto.hpp"
#include "Intervalo.hpp"
#include "SweepLineStatus.hpp"
#include <utility> 

/**
 * My compare functions
 * */

struct CustomCompare
{
    bool operator()(const Ponto &p1 , const  Ponto &p2) const {
   
        return p1.y < p2.y;
    }
};

struct CustomCompare3
{
    bool operator()(const SweepLineStatus &i, const SweepLineStatus &j) const {
   
        return i.p_.x < j.p_.x;
    }
};

struct CustomCompare2
{
    bool operator()(const Intervalo &i, const Intervalo &j  ) const {
   
        return i.A.y > j.A.y || i.B.y > j.B.y;
    }
};

bool sortbysec(const std::pair <Ponto, bool> &a, 
              const std::pair <Ponto, bool> &b) 
{ 
    return (a.first.x < b.first.x ); 
};
#endif //__UTILS_HPP__