#include "Intersects.hpp"
#include <iostream>
#include "Ponto.hpp"
#include <algorithm>  


int lado(const Ponto p1_, const Ponto p2_, const Ponto p3_)
{
    int s = p1_.x*p3_.y - 
            p1_.y*p3_.x +
            p1_.y*p2_.x - 
            p1_.x*p2_.y + 
            p3_.x*p2_.y - 
            p3_.y*p2_.x;

    if(s < 0)
    {
        return -1;
    } else if(s > 0) {
        return 1;
    } else {
        return 0;
    }
}

bool intercaoRetangulos(const Ponto A, 
                        const Ponto B, 
                        const Ponto C, 
                        const Ponto D)
{
    Ponto P = Ponto(std::min({A.x, B.x}), std::min({A.y, B.y}));
    Ponto Q = Ponto(std::max({A.x, B.x}), std::max({A.y, B.y}));

    Ponto P1 = Ponto(std::min({C.x, D.x}), std::min({C.y, D.y}));
    Ponto Q1 = Ponto(std::max({C.x, D.x}), std::max({C.y, D.y}));

    return( (Q.x >= P1.x) && (Q1.x >= P.x) &&
            (Q.y >= P1.y) && (Q1.y >= P.y) );
}


bool seInterceptam(const Ponto A, const Ponto B, const Ponto C, const Ponto D)
{
    int abc, abd, cda, cdb;

    if(!intercaoRetangulos(A, B, C, D))
        return false;
    
    abc = lado(A, B, C);
    abd = lado(A, B, D);
    cda = lado(C, D, A);
    cdb = lado(C, D, B);
    
    return( (abc * abd < 0) && (cda * cdb < 0));
}


double calc_den(const Ponto p1, const Ponto p2, 
                const Ponto p3,const Ponto p4)
{
    double den = (p4.y - p3.y)*(p2.x - p1.x) -
                (p4.x - p3.x)*(p2.y - p1.y);
    return(den);
}

double * calc_num(const Ponto p1, const Ponto p2, 
                const Ponto p3,const Ponto p4)
{
    double num_u = ((p4.x - p3.x)*(p1.y - p3.y) -
                (p4.y - p3.y)*(p1.x - p3.x));
         

          double num_v = (p2.x - p1.x)*(p1.y - p3.y) - 
                    (p2.y - p1.y)*(p1.x - p3.x);

        double *v = new double(2);
        v[0] = num_u;
        v[1] = num_v;
    
        return(v);       
}

bool left(const Ponto p1, const Ponto p2, const Ponto p3)
{
    return( (p1.x*(p2.y - p3.y) + p2.x*(p3.y - p1.y) + p3.x*(p1.y - p2.y)) > 0);
}


bool cross(const Ponto p1, const Ponto p2, const Ponto p3, const Ponto p4)
{
    return( (left(p1, p2, p3) ^ left(p1, p2, p4)) 
            && (left(p3, p4, p1) ^ left(p3, p4, p2))  );
}

// bool PontoIntersecao( const Ponto A, 
//                     const Ponto B, 
//                     const Ponto C, 
//                     const Ponto D, 
//                     Ponto &I)
               
// {
//     double s, t, denom; 

//     if(!seInterceptam(A, B, C, D))
//     {
//         return false;
//     }

//     double den = ((B.y - A.y) * (D.x - C.x) - (D.y - C.y) * (B.x - A.x));

//     if(den = 0)
//     {
//         return false;
//     }

//     s = (A.x * (D.y - C.y) + C.x * (A.y - D.y) + D.x * (C.y - A.y)) / den;

//     t = - (A.x * (C.y - B.y) + B.x * (A.y - C.y) + C.x * (B.y - A.y) / den;

//     if ((s > 0) && (s < 1) && (t > 0) && (t < 1))
//     {
//         I.x = A.x + s * (B.x - A.x);
//         I.y = A.y + s * (B.y - A.y);
//         return true;
//     } else {
//         return false;
//     }
// }
