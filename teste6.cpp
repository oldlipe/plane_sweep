#include <iostream>
#include <algorithm>  

struct Ponto
{
    int x, y;
    Ponto(int _x, int _y):x(_x),y(_y){};
};

int main()
{
    Ponto P = Ponto(std::min({0,4,3, -2}), std::min({-1,2}));

    std::cout << P.x << " " << P.y;

}