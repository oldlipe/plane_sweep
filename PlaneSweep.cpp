#include "PlaneSweep.hpp"
#include <iostream>
#include <iterator>
#include "Utils.hpp"


bool PlaneSweep::sIntersect(const std::vector<SweepLineStatus> Total)
{
    std::set<Intervalo, CustomCompare2> T;
    for(auto &i : Total)
    {
        if(i.esq_)
        { 
            T.insert({i.s_.A, i.s_.B});      

            auto pred = std::prev(T.find({i.s_.A, i.s_.B}));
            auto suc = T.upper_bound({i.s_.A, i.s_.B});
            
            if( 
                (pred != T.end()   && 
                    suc != T.end() &&  
                    seInterceptam(suc->A, suc->B, i.s_.A, i.s_.B)) 
                || (pred != T.end() && 
                    suc != T.end()  && 
                    seInterceptam(pred->A, pred->B, i.s_.A, i.s_.B))
                )
            {      
                std::cout << "Intersecção no primeiro if" << std::endl;

                return(true);
            }
        } else {
     
            auto pred = std::prev(T.find({i.s_.A, i.s_.B }));
            auto suc = T.upper_bound({i.s_.A, i.s_.B });
 
            if(pred != T.end() && 
                suc != T.end() && 
                seInterceptam(i.s_.A, i.s_.B, pred->A, suc->A))
            {
                std::cout << "Intersecção no segundo if" << std::endl;           
                
                return(true);
            }
        T.erase({i.s_.A, i.s_.B});
      }
    }
    std::cout << "Sem intersecção" << std::endl;
    return(false);
}

int PlaneSweep::bIntersect(std::priority_queue<SweepLineStatus, 
                        std::vector<SweepLineStatus>, 
                        CompareHeight > Total)
{
    int segs = 0;
    std::set<Intervalo, CustomCompare2> T;
    Ponto *i;
    while(!Total.empty())
    {  
        auto &i = Total.top();

        if(i.esq_)
        { 
            T.insert({i.s_.A, i.s_.B});      

            auto pred = std::prev(T.find({i.s_.A, i.s_.B}));
            auto suc = T.upper_bound({i.s_.A, i.s_.B});
            
             if( 
                (pred != T.end()   && 
                    suc != T.end() &&  
                    seInterceptam(suc->A, suc->B, i.s_.A, i.s_.B)) 
                || (pred != T.end() && 
                    suc != T.end()  && 
                    seInterceptam(pred->A, pred->B, i.s_.A, i.s_.B))
                )
            {      
                std::cout << "Intersecção no primeiro if" << std::endl;

                segs++;
            }
        }

        //     if( 
        //         (pred != T.end()   && 
        //             suc != T.end() &&  
        //             seInterceptam(i.s_.A, i.s_.B, suc->A, suc->B)))
        //     {
        //         std::cout << "X: " << i.p_.x << " Y: " << i.p_.y << std::endl;
        //         segs++;  
        //     }
        //     else if(
        //         (pred != T.end() && 
        //             suc != T.end()  && 
        //             seInterceptam(i.s_.A, i.s_.B, pred->A, pred->B)))
        //     {
        //         std::cout << "X: " << i.p_.x << " Y: " << i.p_.y << std::endl;
        //         segs++;
        //     }
        // }
        else
        {
            auto pred = std::prev(T.find({i.s_.A, i.s_.B }));
            auto suc = T.upper_bound({i.s_.A, i.s_.B });
 
            if(pred != T.end() && 
                suc != T.end() && 
                seInterceptam(i.s_.A, i.s_.B, pred->A, suc->A))
            {
                std::cout << "Intersecção no segundo if" << std::endl;
                std::cout << "X: " << i.p_.x << " Y: " << i.p_.y << std::endl;
                
                segs++;
            }

            T.erase({i.s_.A, i.s_.B});
        }
        Total.pop();
    }
    std::cout << "Total de " << segs << " interseccionados" << std::endl;
    return segs;
}